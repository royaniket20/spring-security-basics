package com.aniket.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    //Jira Done
    @GetMapping("/welcome")
    public  String welcome()
    {
        return "Hello  World";
    }

    @GetMapping("/public")
    public  String Public()
    {
        return "This is a public API";
    }

    @GetMapping("/accounts")
    public  String Accounts()
    {
        return "This is a Accounts API";
    }



}
